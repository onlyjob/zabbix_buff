#!/usr/bin/perl
=head1 NAME

zabbix_buff.pl -- feed cached output of plugins to zabbix_agent UserParameter

=head1 COPYRIGHT

Copyright: 2011-2012 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE GPL-3+

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 VERSION

2012.1022

=cut
use 5.8.0;
use strict;
use autouse 'Pod::Usage'=>qw(pod2usage);
use Getopt::Long;
use Storable qw(lock_retrieve fd_retrieve lock_store store_fd);
use Fcntl qw(:flock LOCK_EX LOCK_UN LOCK_NB);

my %OPT=(             # defaults
    'cache_dir'       => '/tmp/zabbix_buff/',
    'update'          => 300,                              # seconds to use cached data for.
    'sleep'           => 20,                               # seconds to wait for lock, when data file is updating.
    'regex'           => q{(\S+)\s*:\s*(.*?)$},
);
GetOptions( \%OPT,
    ,'help|h|?' ,'man'
    ,'cache_dir|c=s'
    ,'debug|d'
    ,'no-native'
    ,'regex=s'
    ,'update|u=i'
    ,'verbose|v'
) or pod2usage(-exitval=>0, -verbose=>0);
pod2usage(-exitval=>0, -verbose=>1) if $OPT{'help'};
pod2usage(-exitval=>0, -verbose=>2, -noperldoc=>0) if $OPT{'man'};

use subs qw(warn die);
sub warn {
    print {*STDERR} @_;
}
sub die {
    close $OPT{'FH'} if defined $OPT{'FH'};
    unlink $OPT{'db_file'} unless -s $OPT{'db_file'};           # do not leave empty files behind
    warn @_;
    exit 1;
}
$SIG{INT}=$SIG{TERM}=sub { die("I: Terminated."); };

$OPT{'item'}=shift @ARGV
    or die "E: 'item' must be specified\n";

$OPT{'plugin'}=shift @ARGV
    or die "E: 'plugin' must be specified\n";

$OPT{'cache_dir'}=~s{/*\Z}{/};
unless(-d $OPT{'cache_dir'}){
    warn qq{I: making directory "$OPT{'cache_dir'}"\n} if $OPT{'verbose'};
    mkdir $OPT{'cache_dir'}, 750;
}

# construct cache-file name from plugin name and its argument
$_=join q(__),($OPT{'plugin'},$ARGV[0]);
s{[^a-zA-Z0-9_-]}{_}smg;                # clean file name from unwanted characters
$OPT{'db_file'}=$OPT{'cache_dir'}.$_;

$OPT{'db_lastmod'}=24*60*60*(-M $OPT{'db_file'});               # modified N seconds ago
if ($OPT{'db_lastmod'}-$OPT{'update'}>0
    or not -f -s $OPT{'db_file'}
){
    if (-f -s $OPT{'db_file'}){
        warn "I: New data needed (existing file modified $OPT{'db_lastmod'} sec. ago).\n" if $OPT{'verbose'};
    }else{
        warn "I: New data needed (creating new $OPT{'db_file'}).\n" if $OPT{'verbose'};
    }
    $OPT{'mode'}=$OPT{'db_lastmod'}==0
        ? '>'                               # create new file
        : '+<'                              # update existing file
    ;
    open $OPT{'FH'},$OPT{'mode'},$OPT{'db_file'}
        or die "E: unable to open file $OPT{'db_file'} to store updates.\n";

    warn qq{I: Trying to lock "$OPT{'db_file'}" ...} if $OPT{'verbose'};
    if(1==flock $OPT{'FH'},LOCK_EX | LOCK_NB){    # lock obtained, ready for update
        warn " success.\n" if $OPT{'verbose'};

        ## calling plugin to retrieve updated information
        $_='';
        if ($OPT{'plugin'}=~m{\.(?:pl|pm)\Z} and not $OPT{'no-native'}){
            warn qq{I: Calling plugin "$OPT{'plugin'}" (native mode).\n} if $OPT{'verbose'};
            warn "I: Plugin arguments: ".join(q( ),@ARGV)."\n" if $OPT{'verbose'} and $#ARGV>=0;
            # try to load as native plugin in Perl.
            $0.=q{:}.$OPT{'plugin'};
            $_=do qq{$OPT{'plugin'}};
            # non-Perl plugins will return error in $@
        }else{
            warn qq{W: executing "$OPT{'plugin'}" (exec mode).\n} if $OPT{'verbose'};
            $_=qx($OPT{'plugin'} $ARGV[0] $ARGV[1])
                or die "E: unable to execute $OPT{'plugin'}\n";
            $ARGV[0]={m{$OPT{'regex'}}smg};
        }

        if(ref($ARGV[0]) eq 'HASH' and keys %{$ARGV[0]}){
            warn "I: Storing data.\n" if $OPT{'verbose'};
            store_fd($ARGV[0], $OPT{'FH'});
            #lock_store($ARGV[0], $OPT{'FH'});
        }else{
            warn "E: No data retrieved, check plugin.\n";
        }

        flock $OPT{'FH'},LOCK_UN;                 # unlock file handle before close
    }else{                      # db_file is locked by other process, wait and read.
        warn "already locked, waiting for new data.\n" if $OPT{'verbose'};

        while($OPT{'sleep'}-=1){
            sleep 1;
            if(1==flock $OPT{'FH'},LOCK_EX | LOCK_NB){  # no more lock, file is updated
                warn "I: released, will read in a sec...\n" if $OPT{'verbose'};
                #sleep 1;       # precaution, let other process to finish writing.

                $ARGV[0]=fd_retrieve($OPT{'FH'})
                    or die "E: can't retrieve data\n";
                last;
            }
            warn "I: Waiting for lock ($OPT{'sleep'})." if $OPT{'verbose'};
        }
    }
    close $OPT{'FH'};

}else{
    die "E: Can't retrieve data from $OPT{'db_file'}\n" unless -s $OPT{'db_file'};
    $ARGV[0]=lock_retrieve($OPT{'db_file'});
    warn "I: Retrieving data from cache file $OPT{'db_file'} (modified $OPT{'db_lastmod'} sec. ago).\n" if $OPT{'verbose'};
}

if($OPT{'debug'}){        # print all the data and configuration
eval <<'DUMPER'
    use Data::Dumper;
    print {*STDERR} Dumper \@ARGV,
                           \%OPT;
DUMPER
}

# print only requested value
if(defined $ARGV[0]->{$OPT{'item'}}){
    print $ARGV[0]->{$OPT{'item'}};
    warn "I: $OPT{'plugin'} | $OPT{'item'} : $ARGV[0]->{$OPT{'item'}}\n"
        if $OPT{'verbose'};
}else{
    print 'ZBX_NOTSUPPORTED';
}

__END__

=head1 SYNOPSIS

zabbix_buff.pl [ B<options> ] B<key> B<plugin_executable> [ B<plugin_arguments> ]

=head1 DESCRIPTION

This program is to be used as Zabbix UserParameter
to cache and filter the output of another command or plugin.

=head1 USAGE

In /etc/zabbix_agentd.conf or in /etc/zabbix_agentd.conf.d/

    UserParameter=hdd[*],perl /usr/local/bin/zabbix_buff.pl "$2" /usr/local/bin/zagg/zhdd.pl "$1" || echo -n "ZBX_NOTSUPPORTED"

If called with key "hdd[sda,Temperature_Celsius]" zabbix_buff
will execute "zhdd.pl sda", cache its output (all parameters)
and return value of "Temperature_Celsius".

    || echo -n "ZBX_NOTSUPPORTED"

Is required to inform Zabbix about failure or incorrect parameter.

=head1 OPTIONS

=over 8

=item B<--help>

Print a brief help message and exit.

=item B<-c>,B<--cache_dir>=/tmp/zabbix_buff

Directory to store cached plugins' output (will be created if not exist).

=item B<--debug|d>

Debug mode: will dump all internal variables to STDERR.

=item B<--no-native>

Do not try to DO Perl plugins, run as child process instead.

=item B<-u>,B<--update>=300

Seconds to use cached data for. By default new data will be requested from
plugin every 300 seconds.

=item B<-v>,B<--verbose>

Verbose processing: will print what's being done to STDERR.

=back

=head1 USE CASE

Consider the following trivial example where UserParameter is extracted from
corresponding file in virtual file system:

    UserParameter=linux.mm.ksm[*],cat "/sys/kernel/mm/ksm/$1" || echo -n "ZBX_NOTSUPPORTED"
    UserParameter=proc.sys.vm[*],cat "/proc/sys/vm/$1" || echo -n "ZBX_NOTSUPPORTED"

Note that fallback to ZBX_NOTSUPPORTED is necessary to avoid returning
empty string for non-existent parameters. Zabbix agent is ignoring exit code
and return empty string to server even if UserParameter command returned
nothing or crashed.

The following slightly more complicated example get UserParameter from list
of parameters read from small file in virtual file system.

    UserParameter=proc.meminfo[*],perl -0ne 'print m{^$1:\s+(\d+)}sm ? $$1 : q{ZBX_NOTSUPPORTED}' /proc/meminfo

The above example is still efficient enough. However in some situations
UserParameters may need to be extracted from output of command-line
utility which takes long time to execute and/or provide many parameters.
For example "mysqladmin variables" may return over 300 parameters. If we
poll only 10% of them (i.e. 30) every minute then mysqladmin will be
invoked 30 times per minute. Although not efficient this still may be
acceptable when command can reliably finish withing short time and
generate low overhead during its execution. In many cases when command
is slow (for example if it fetches a slow web page to parse parameters)
or if it block hardware or simply can't run in parallel with itself.

Quite effective way to feed multiple parameters to Zabbix is to use
"zabbix_sender" utility which can send multiple parameters at once if
corresponding items are configured as "Zabbix trapper". This very
efficient method still have some limitations and generally is more
difficult to set up. For example the following must be considered for
polling with sender/trapper:

  * Item keys must use no MACROs as server will receive raw keys from
    sender. zabbix_sender can't update items that require MACROs expansion.
    i.e. zabbix_sender and discovery (item prototypes) do not mix.
    This is severely impair maintainability of complex templates.
    (ZBX-5332,ZBX-5715)
  * Items may be restricted for updates from trusted host(s) only:
    at the moment (Zabbix_2.0.3) it can be done only per-item.
  * Polling interval can't be controlled on server side. When zabbix_sender
    pushes data to server all items are updated at once. In some scenarios
    it may be desirable to poll values at different rate, for example
    some items may be updated every minute while others may be fetched once
    an hour or once a day. If many parameters including those that are not
    expected to change frequently are sent by zabbix_sender
    too often this may create extra load to Zabbix server and store
    much of unnecessary data.
  * Regular invocation of zabbix_sender need to be arranged by dedicated
    UserParameter or by cron.

As alternative to the above options this caching application can be used,
see I<DESCRIPTION> and I<USAGE>.

=head1 PLUGINS

Simplest plugin is just an executable that print list of parameters,
formatted as follows:

    parameter_one   : 1.0
    parameter_two   : two

Where
    each parameter on its own line;
    each line begins with parameter name;
    parameter name contains no spaces;
    parameter value separated from name by ':' character;
    separator may be surrounded by spaces;
    value is a free-form string ends with end-of-line;

=head1 NATIVE PLUGINS

Plugins written in Perl will be interpreted by the same Perl instance used
to run zabbix_buff if plugin name ends with ".pl" or ".pm".

Native plugin is expected to return HASH (associative array)
with parameters and corresponding values in $ARGV[0].

This is easy to do with the following code:

    if(caller()){       # called as native plugin
        $ARGV[0]=\%data;
    }

=head1 EXAMPLES

Override cache location and duration:
    perl zabbix_buff.pl --cache_dir=/tmp/zabbix_buff --update=300 kernel_device ./plugins/zhdd.pl sda

Invocation without plugin:
    ./zabbix_buff.pl --regex="^(\S+)\s*: \s*(.*?)$" MemFree -- cat /proc/meminfo

Standalone plugin invocation:
    perl zfin_asxi.pl | grep XSJ.name | sed 's/[^:]*:\s*//'

=head1 SEE ALSO

Use perldoc(1) to read plugins' documentation and examples.

=head1 TODO

Consider running as zabbix_buff as daemon.

=cut
