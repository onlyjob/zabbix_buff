#!/usr/bin/perl
=head1 NAME

zfin_asxi.pl -- zabbix_buff plugin to fetch Australian Securities
Exchange (ASX) Indexes.

=head1 COPYRIGHT

Copyright: 2011-2012 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE GPL-3+

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 VERSION

2012.1022

=cut
use 5.8.0;
use strict;
use autouse 'JSON::PP'=>qw(encode_json);
use LWP::UserAgent;
my $ua = LWP::UserAgent->new(
    max_redirect=>3,
    max_size=>1024*128,
);
my $res=$ua->get('http://www.asx.com.au/asx/statistics/indexInfo.do');
die "E: [$0] ".$res->status_line."\n" unless $res->is_success;

$_=$res->is_success
    ? $res->content
    : undef
;

s{<(head|script).*?</\1>\s*}{}sg;
s{\s*</?(tbody|img|div|span)[^>]*>}{}sg;
s{\s+(class|scope|cellspacing|nowrap)="[^"]*"}{}sg;
s{\s*(</?(?:td|th)>)\s*}{$1}sg;

my %data;
my %codes;
while(m{
        <tr><td></td><td>([^<>]+)</td>      # name
        <td>([^<>]+)</td>                   # code
        <td>([0-9.,]+)</td>                 # last
        <td>(-?[0-9.,]+)</td>               # movement
        <td>([0-9.,]+)</td></tr>            # close
    }sgx
){
    my $code=$2;
    $codes{$code}=1;

    $data{$code.'.name'}    = $1;
    $data{$code.'.code'}    = $2;
    $data{$code.'.last'}    = $3;
    $data{$code.'.movement'}= $4;
    $data{$code.'.close'}   = $5;

    foreach (keys %data){
        next unless m{\.(last|close|movement)};
        $data{$_}=~s{,}{};
    }
}

if(shift(@ARGV) eq '--discovery'){
    my %d=(data=>[]);
    for (sort keys %codes){
        push $d{'data'},{'{#IDX}'=>$_};
    }
    print encode_json(\%d);
}elsif(caller()){
    $ARGV[0]=\%data;
}else{                                          # standalone invocation
    for my $key(sort keys %data){
        print $key,"\t" x (3-int(length($key)/8))
             ,': '.$data{$key}
             ,"\n"
        ;
    }
}
