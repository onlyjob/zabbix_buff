#!/usr/bin/perl
=head1 NAME

zmysql.pl -- MySQL plugin for zabbix_buff

=head1 COPYRIGHT

Copyright: 2011-2012 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE GPL-3+

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 VERSION

2012.1022

=cut
use 5.8.0;
use strict;

my $cmd_sctl_var='/usr/bin/mysqladmin variables';
my $cmd_sctl_stat='/usr/bin/mysqladmin status';

$_=qx{$cmd_sctl_var @ARGV};
die "E: [$0] no data, possibly no output from $cmd_sctl_var\n"
    if $_ eq '';

my %data=m{^\|\s*([^|]+?)\s*\|\s*([^|]+?)\s*\|$}smg;
delete $data{'Variable_name'};                      # removing parsed header

$_=qx{$cmd_sctl_stat @ARGV};
die "E: [$0] no data, possibly no output from $cmd_sctl_stat\n"
    if $_ eq '';

my %data_stat=m{\s*([^:]+):\s*([0-9.]+)}smg;
@data{map lc,keys %data_stat}=values %data_stat;    # merge and lowercase status variables

for (keys %data){                   # renaming value names
    my $key=$_;
    s{\s+}{_}g or next;
    $data{$_}=$data{$key};
    delete $data{$key};
}

if(caller()){
    $ARGV[0]=\%data;
}else{                              # standalone invocation
    for my $key(sort keys %data){
        print $key,"\t" x (6-int(length($key)/8))
             ,': '.$data{$key}
             ,"\n"
        ;
    }
}
