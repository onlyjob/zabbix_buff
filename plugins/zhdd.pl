#!/usr/bin/perl
=head1 NAME

zhdd.pl: HDD plugin for zabbix_buff

=head1 COPYRIGHT

Copyright: 2011-2013 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE GPL-3+

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 SUDO

sudo(8) need the following setting:

    zabbix  ALL=NOPASSWD: /usr/sbin/smartctl

=head1 VERSION

2013.0317

=cut
use 5.8.0;
use strict;
use autouse 'JSON::PP'=>qw(encode_json);

my $hdd=shift @ARGV
    or die "E: [$0] Expected argument (device like 'sda').\n";

if($hdd eq '--discovery'){
    my %data=(data=>[]);
    opendir(my $DH, '/dev/disk/by-path')
    or die "E: [$0] no path /dev/disk/by-path\n";
        while(readdir $DH){
            next if $_ eq '.' or $_ eq '..';
            next if m{-part\d+$};
            push $data{'data'},{'{#HDD}'=>q{disk/by-path/}.$_};
        }
    closedir $DH;
    print encode_json(\%data);
    exit;
}

# Try to find real device name
$hdd=~s{^/dev/}{};
unless($hdd=~m{^[^/]+\Z} and -b '/dev/'.$hdd){
    for( readlink '/dev/'.$hdd
        ,readlink '/dev/disk/'.$hdd
        ,readlink '/dev/disk/by-path/'.$hdd
        ,readlink '/dev/disk/by-uuid/'.$hdd
        ,readlink '/dev/disk/by-id/'.$hdd
    ){
        next if $_ eq '';
        if(s{^\.\./\.\./}{}){
            $hdd=$_;
            last;
        }
    }
}
# Check if device name found or die
die "E: [$0] unable to find device $hdd\n" unless -e '/dev/'.$hdd;

my $cmd_sctl='/usr/sbin/smartctl';
$_=qx{sudo -n $cmd_sctl --info --health --attributes --nocheck=standby --log=error /dev/$hdd};
die "E: [$0] no data, possibly no output from $cmd_sctl\n" if $_ eq '';

my %patterns=(
    # --info
    'Model_Family'            =>  qr/Model Family:\s*(.+?)\s*$/sm,
    'Device_Model'            =>  qr/Device Model:\s*(.+?)\s*$/sm,
    'Serial_Number'           =>  qr/Serial Number:\s*(.+?)\s*$/sm,
    'Firmware_Version'        =>  qr/Firmware Version:\s*(.+?)\s*$/sm,
    ##'Local_Time'            =>  qr/Local Time is:\s*(.+?)\s*$/sm,
    'User_Capacity'           =>  qr/User Capacity:\s*([0-9,]+)/sm,
    'Warning'                 =>  qr/==>\s*WARNING:\s*(.*?)\n\n/sm,

    # data returned by DVD drives
    'Product'                 =>  qr/Product:\s*\s*(.+?)\s*$/sm,
    'Vendor'                  =>  qr/Vendor:\s*\s*(.+?)\s*$/sm,

    # --health
    'Self_Check'              =>  qr/SMART overall-health self-assessment test result:\s*(.+?)\s*$/sm,

    # --log=error
    'Log_Error_Count'         =>  qr/SMART\s+Error\s+Log\s+Version:\s+\d*\s+(?:ATA\s+Error\s+Count:\s*)*(\d+|No Errors Logged)/sm,

    # --attributes
    'Start_Stop_Count'        =>  qr/Start_Stop_Count        \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(.*?)$/smx,
    'Power_Cycle_Count'       =>  qr/Power_Cycle_Count       \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(.*?)$/smx,
    'Power_On_Hours'          =>  qr/Power_On_Hours          \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(.*?)$/smx,
    'Temperature_Celsius'     =>  qr/Temperature_Celsius     \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(\d+)/smx,
    'Offline_Uncorrectable'   =>  qr/Offline_Uncorrectable   \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(.*?)$/smx,
    'Reallocated_Sector_Ct'   =>  qr/Reallocated_Sector_Ct   \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(\d+)$/smx,
    'UDMA_CRC_Error_Count'    =>  qr/UDMA_CRC_Error_Count    \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*\S* \s*(\d+)$/smx,

);

my %data;
for my $key(keys %patterns){
    $data{$key}=$1 if /$patterns{$key}/;
}

## correct gathered data
$data{'Warning'}=~s{\n}{ }sg;
$data{'Log_Error_Count'}=0 if $data{'Log_Error_Count'} eq 'No Errors Logged';
$data{'User_Capacity'}=~s{,}{}g if defined $data{'User_Capacity'};
if (defined $data{'Product'}){              # DVD drive
    $data{'Device_Model'}=$data{'Product'};
}
$data{'kernel_device'}="/dev/$hdd";         # add device name as known to kernel
                                            # they are often change on reboot.

if(caller()){       # called as native plugin
    $ARGV[0]=\%data;
}else{              # standalone invocation
    for my $key(sort keys %data){
        print $key
             ,"\t" x (3-int(length($key)/8))
             ,q{: }.$data{$key},"\n";
    }
}
